FROM python:latest

RUN python -m pip install --upgrade pip
WORKDIR /django
COPY . /django
RUN python -m pip install -r requirements.txt
